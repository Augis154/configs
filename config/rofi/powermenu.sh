#!/bin/bash

session=`loginctl session-status | awk 'NR==1{print $1}'`

POWER=""
RESTART=""
LOGOUT=""
RES=`echo "$POWER|$RESTART|$LOGOUT|$LOCK" | rofi -dmenu -yoffset -104 -sep "|" -theme powermenu`
[ "$RES" = "$POWER" ] && systemctl poweroff
[ "$RES" = "$RESTART" ] && systemctl reboot
[ "$RES" = "$LOGOUT" ] && loginctl terminate-session $session