install_all: install
	sudo pacman -S lolcat firefox neofetch discord kdeconnect thunar htop vifm kate arduino gimp inkscape deadbeef virtualbox qemu cmatrix sl ark spectacle libreoffice bluez-utils lutris sysstat steam
	yay -S minecraft-launcher heroic-games-launcher-bin brave-bin breeze-hacked-cursor-theme moc-pulse

install:
	sudo cp -R pacman.conf /etc
	sudo pacman -Syu i3 xorg xorg-xinit xorg-server kitty conky xss-lock fish dunst jq lxappearance plasma plasma-nm plasma-pa feh rofi alsa-utils pavucontrol ttf-font-awesome lxsession gvfs
	yay -S i3lock-fancy-git ntfs-3g ttf-ms-fonts ttf-weather-icons
	make copy

copy:
	rm -rf ~/.moc
	mkdir ~/.moc
	cp -R moc/* ~/.moc
	cp -R config/* ~/.config
	cp -R Wallpapers ~/
	cp -R assets.zip ~/
	sudo cp -R bins/* /usr/bin
	sudo cp -R pacman.conf /etc
	
cpy:
	cp -R config/* ~/.config
