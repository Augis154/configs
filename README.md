# Configs

i3, Polybar, Kitty, Alacritty, Dunst, Conky, MOC configs

Run `make copy` to copy configs and other files where they belong

Run `make cpy` to copy configs only

Run `make install` to install dependencies

Run `make install_all` to install dependencies and some useful programs
(there is a chance that there is some missing dependencies)
